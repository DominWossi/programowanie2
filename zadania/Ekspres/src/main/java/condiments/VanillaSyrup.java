package condiments;

public class VanillaSyrup implements Condiment {
    public String getCondimentName() {
        return "Vanilla Syrup";
    }
}
