package condiments;

public class BrownSugar implements Condiment {
    public String getCondimentName() {
        return "Brown Sugar";
    }
}
