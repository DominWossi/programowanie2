package condiments;

public class WhippedCream implements Condiment {
    public String getCondimentName() {
        return "Whipped Cream";
    }
}
