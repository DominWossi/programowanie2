public class Teacher {
    String name;
    String surname;

    @Override
    public String toString() {
        return "Teacher{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }

    public Teacher(Teacher teacher) {
        this.name = teacher.name;
        this.surname = teacher.surname;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Teacher(String name, String surname) {

        this.name = name;
        this.surname = surname;
    }
}
