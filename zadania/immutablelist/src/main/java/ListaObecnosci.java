import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ListaObecnosci {
    private final List<String> lista;
    private final Teacher nauczyciel;
    private final Date data;

    public ListaObecnosci(List<String> lista, Teacher nauczyciel, Date date) {
        this.lista = new ArrayList<>(lista);
        this.nauczyciel = new Teacher(nauczyciel);
        this.data = date;
        id = id_counter;
        id_counter++;
    }

    @Override
    public String toString() {
        return "ListaObecnosci{" +
                "lista=" + lista +
                ", nauczyciel=" + nauczyciel +
                ", data=" + data +
                ", id=" + id +
                '}';
    }

    private static int id_counter;
    private final int id;

    public List<String> getLista() {
        return new ArrayList<>(lista);
    }

    public Teacher getNauczyciel() {
        return new Teacher(nauczyciel);
    }

    public Date getData() {
        return new Date(data.getTime());
    }

    public int getId() {
        return id;
    }
}
