import java.sql.Date;
import java.time.LocalDate;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {
        ListaObecnosci obecnosci  = new ListaObecnosci(Collections.singletonList("Uczen"),new Teacher("Adam", "Adam"), Date.valueOf(LocalDate.now()));
        System.out.println(obecnosci);
        obecnosci.getNauczyciel().name = "Michał";
        System.out.println(obecnosci);
    }
}
