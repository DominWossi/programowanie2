public final class OperatingSystemImpl implements OperatingSystem {
    private static  OperatingSystemImpl instance;
    private OperatingSystemImpl() {
    }

    public static final OperatingSystemImpl getInstance() {
        if (instance == null) {
            instance = new OperatingSystemImpl();
        }
        return instance;
    }
    public void login(String username, String password) {
        System.out.println("Zalogowano!");
    }

    public void logout() {
        System.out.println("Wylogowano!");
    }
}
