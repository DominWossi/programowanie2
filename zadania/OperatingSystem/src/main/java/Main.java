public class Main {
    public static void main(String[] args) {
        OperatingSystem system = new ProxyOperatingSystem();
        performLogin(system);
        performLogout(system);
    }

    public static void performLogin(OperatingSystem system) {
        system.login("Adam", "Adam");
    }

    public static void performLogout(OperatingSystem system) {
        system.logout();
    }
}
