public interface OperatingSystem {
    void login(String username, String password);
    void logout();
}
