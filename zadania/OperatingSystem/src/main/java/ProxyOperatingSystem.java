public class ProxyOperatingSystem implements OperatingSystem{
    OperatingSystem operatingSystem;

    public void login(String username, String password) {
        if(username.equals(password)) {
            throw new SameUsernameAndPasswordException();
        } else {
            operatingSystem.login(username, password);
        }
    }

    public void logout() {
        operatingSystem.logout();
    }
}
